
import java.util.*;
import java.math.*;
import java.text.*;
// a little class which is intended to be used to replicate a simplified online order checkout process. 
// it currently randomly generates some order data and prints it out.
public class SRCJavaTest{
    public static Product[] products;
    public static void main(final String[] args){
        SRCJavaTest runner = new SRCJavaTest();     
        runner.run();
    }

    public void run(){
         products = buildProductSets();

         String[] names = new String[]{"Earl","Haidar","Giles","Harlee","Anil","Samara","Lexie","Khalid","Bogdan","Krishan","Dustin"};
         Order o = new Order(new Customer(names[(int)Math.floor(Math.random() * names.length)]));

         for(int i=(int)Math.ceil(Math.random() * 10); i>=0;  i--){
            int productIndex = (int)Math.floor(Math.random() * products.length);
            Item s = products[productIndex].skus.get((int)Math.floor(Math.random() * products[productIndex].skus.size()));
            CommandFactory.getIntense().getAddProductToOrderCmd().addProduct(s,(int)Math.ceil(Math.random() *3), o);
         }

         CommandFactory.getIntense().getShippingCalculator().calculate(o);
         CommandFactory.getIntense().getOrderTotalCalculator().calculate(o);
         o.printOrder();

    }

    public class Product{
        public String id;

        public double weight;

        public double height;
        public double width;
        public double depth;

        public BigDecimal price;

        public List<Item> skus;

        public Product(String id, double weight, double height, double width, double depth, BigDecimal price){
            this.id = id;
            this.weight = weight;
            this.height = height;
            this.width = width;
            this.depth = depth;
            this.price = price;
        }

    }
    public class Item{
        public Product p;
        public String colour;
        public String size;

        public Item(Product product, String colour, String size){
            this.p = product;
            this.colour = colour;
            this.size = size;  
        }   
        
    }

    public class Order{
        public Customer customer;
        public String status;
        public Address shippingAddress;
        public Address billingAddress;
        public List<OrderItem> orderItems;
        public OrderTotal orderTotal;

        public Order(Customer customer){
            this.customer = customer;
            this.status = "new";
        }

        public void printOrder(){
            System.out.println(String.format("%s - items: %s ", new Object[]{customer.name,orderItems.size()}));
            for(int i=0; i<orderItems.size(); i++){
                orderItems.get(i).printItem();
            }
            System.out.println(String.format("\nitem total:\t%s\nshipping:\t%s\ntotal:\t\t%s",new Object[]{
                NumberFormat.getCurrencyInstance().format(orderTotal.orderSubTotal),
                NumberFormat.getCurrencyInstance().format(orderTotal.shipingCharge),
                NumberFormat.getCurrencyInstance().format(orderTotal.total)
            }));
        }
    }
    
    public static class OrderItem{
        public Item sku;
        public int qty;
        public BigDecimal price;
        public OrderItem(Item sku, int qty, BigDecimal price){
            this.sku = sku;
            this.qty = qty;
            this.price = price;
        }
        public void printItem(){
            System.out.println(String.format("%s\t%s\t%s\t%d\t%s",new Object[]{sku.p.id, sku.colour, sku.size, qty, NumberFormat.getCurrencyInstance().format(price)}));
        }
    }
    public class Address{
        String no;
        String Street;
        String city;
        String state;
        String postocde;
        String country;
    }
    
    public class Customer{
        
        String name;
        
        public Customer(String name){
            this.name = name;
        }
        
    } 

    public static class OrderTotal{
        public BigDecimal orderSubTotal = new BigDecimal(0);
        public BigDecimal shipingCharge = new BigDecimal(0);
        public BigDecimal total = new BigDecimal(0);

        public OrderTotal(){}
    }


    public interface AddProductCmd{
        public Order addProduct(Item sku,int qty, Order order);
    }

    public interface OrderCalculateCmd{
        public Order calculate(Order order);
    }

    public static class CommandFactory {

        private static CommandFactory c; 
        public static CommandFactory getIntense(){
            //ensure only one
            if(c == null){
                return new CommandFactory();
            }
            return c;
        }
        public OrderCalculateCmd getSubTotalCalculator(){
            return new OrderCalculateCmd(){
                public Order calculate(Order order){
                    BigDecimal total = calculateTotal(order.orderItems, new BigDecimal(0));

                    OrderTotal ot = order.orderTotal == null ? new OrderTotal(): order.orderTotal; 
                    ot.orderSubTotal = total;
                    order.orderTotal = ot;
                    return order;
                }
                private BigDecimal calculateTotal(List<OrderItem> items, BigDecimal total){
                    OrderItem oi = items.remove(0);
                    if(items.size() == 0){
                        items.add(oi);
                        return total.add(oi.price.multiply(new BigDecimal(oi.qty)));
                    }
                    BigDecimal t = calculateTotal(items, total).add(oi.price.multiply(new BigDecimal(oi.qty)));
                    items.add(oi);
                    return t;
                }
            };

        }

        public OrderCalculateCmd getOrderTotalCalculator(){
            return new OrderCalculateCmd(){
                public Order calculate(Order order){
                    order.orderTotal.total = order.orderTotal.orderSubTotal.add(order.orderTotal.shipingCharge);
                    return order;
                } 
            };
        }

        public OrderCalculateCmd getShippingCalculator(){
            return new OrderCalculateCmd(){
                public Order calculate(Order order){

                    BigDecimal order_weight_volume = new BigDecimal(0);
                    for(int i=0; i< order.orderItems.size(); i++){
                        OrderItem item = order.orderItems.get(i);
                        Product p = item.sku.p;
                        BigDecimal item_weight_volume = new BigDecimal(p.weight)
                            .multiply(new BigDecimal(p.height))
                            .multiply(new BigDecimal(p.width))
                            .multiply(new BigDecimal(p.depth))
                            .multiply(new BigDecimal(item.qty));
                        order_weight_volume = order_weight_volume.add(item_weight_volume);
                    } 
                    if(order_weight_volume.floatValue() < 100){
                        order.orderTotal.shipingCharge = new BigDecimal(3.20); 
                    }else{
                        if(order_weight_volume.floatValue() < 300){
                            order.orderTotal.shipingCharge = new BigDecimal(3.82);
                        }else{
                            if(order_weight_volume.floatValue() < 500){
                                order.orderTotal.shipingCharge = new BigDecimal(4.37);
                            }else{                           
                                if(order_weight_volume.floatValue() < 800){
                                    order.orderTotal.shipingCharge = new BigDecimal(7.91);
                                }else{
                                    if(order_weight_volume.floatValue() < 1000){
                                        order.orderTotal.shipingCharge = new BigDecimal(11.63);
                                    }else{
                                        order.orderTotal.shipingCharge = new BigDecimal(23.48);                                  
                                    }                                    
                                }
                            }
                        }
                    }

                    return order;
                }
            };
        }

        public AddProductCmd getAddProductToOrderCmd(){

            return new AddProductCmd(){
                public Order addProduct(Item sku,int qty, Order order){
                    if(order.orderItems == null){
                        order.orderItems = new ArrayList<OrderItem>();
                    }
                    order.orderItems.add(new OrderItem(sku, qty, sku.p.price));
                    CommandFactory.getIntense().getSubTotalCalculator().calculate(order);
                    return order;
                } 
            };
        }  
    }


    //buids a sku with a size and colour for each combination for each product of the 100 randomly generated products. 
    private Product[] buildProductSets(){
        String[] colours = {"red", "green", "blue", "yellow","orange", "purple","pink", "brown", "grey", "white", "black"};
        String[] sizes = {"1", "2", "3", "4","5", "6","7", "8", "9", "10", "11"};

        Product[] products = new Product[100];

        for( int i=0; i < 100; i++ ){
            products[i] = new Product("p_" + i*2 , Math.random() * 3 , Math.random() * 4, Math.random() * 5, Math.random() * 15 , new BigDecimal(Math.random() * 63));
            products[i].skus = new ArrayList<Item>();
            for(int j=0; j<colours.length; j++){
                for(int k=0; k<sizes.length; k++){
                    products[i].skus.add(new Item(products[i],colours[j], sizes[k]));
                } 
            }
        }
        return products;
    }
}